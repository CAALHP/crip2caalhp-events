﻿namespace crip2caalhp_events
{
    public class FoundIdFromCripEvent : Event
    {
        public string Id { get; set; }
        public string AuthType { get; set; }
    }
}