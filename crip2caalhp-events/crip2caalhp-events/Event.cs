﻿using System;

namespace crip2caalhp_events
{
    public abstract class Event
    {
        public int CallerProcessId { get; set; }
        public string CallerName { get; set; }
        public DateTime Timestamp { get; set; }

        protected Event()
        {
            Timestamp = DateTime.Now;
        }
    }
}